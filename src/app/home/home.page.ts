import { Component, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild("pageContent", { static: false }) content: IonContent;

  searchTerm: string = "";
  fname: string = "";
  lname: string = "";
  listToggled: boolean = false;
  listItems: string[] = [
    "Item 01",
    "Item 02",
    "Item 03",
    "Item 04",
    "Item 05",
    "Item 06",
    "Item 07",
    "Item 08",
    "Item 09",
    "Item 10",
    "Item 11",
    "Item 12",
    "Item 13",
    "Item 14",
    "Item 15",
    "Item 16",
    "Item 17",
    "Item 18",
    "Item 19",
    "Item 20"
  ];

  constructor() {

  }

  plainButton(): void {
    console.log("Plain Button Pressed");
  }

  buttonWithIcon(buttonName: string): void {
    console.log("Button With Icon Pressed", buttonName);
  }

  makeTeamsCall(href: string): void {
    // (window as any).location = href;
    console.log("Make a Teams Call", href);
  }

  toggleList(): void {
    console.log("TOGGLE LIST");
    this.listToggled = !this.listToggled;
  }

  searchRecords(fromSubmit: boolean): void {
    console.log("SEARCH RECORDS", fromSubmit, this.searchTerm);
  }

  scrollTop(): void {
    console.log("SCROLL TOP");
    this.content.scrollToTop(1500);
  }

  scrollBottom(): void {
    console.log("SCROLL BOTTOM");
    this.content.scrollToBottom(1500);
  }

  scrollUp(): void {
    console.log("SCROLL UP");
    this.content.scrollByPoint(0, -250, 300);
  }

  scrollDown(): void {
    console.log("SCROLL DOWN");
    this.content.scrollByPoint(0, 250, 300);
  }
}
